// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogBuildingEscape, Log, Warning);
