// Copyright © 2021 Derek Fletcher, All rights reserved

#include "BuildingEscape.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BuildingEscape, "BuildingEscape" );

DEFINE_LOG_CATEGORY(LogBuildingEscape);