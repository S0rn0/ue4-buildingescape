// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "DoorController.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UDoorController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDoorController();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

  void OpenDoor(float DeltaTime);
  void CloseDoor(float DeltaTime);

private:

  UPROPERTY(EditAnywhere)
  float DeltaPitch = 0.f;

  UPROPERTY(EditAnywhere)
  float DeltaYaw = 0.f;

  UPROPERTY(EditAnywhere)
  float DeltaRoll = 0.f;

  UPROPERTY(EditAnywhere)
  float OpenSpeed = 0.5f;

  UPROPERTY(EditAnywhere)
  class ATriggerVolume* PressurePlate = nullptr;

  UPROPERTY(EditAnywhere)
  AActor* ActorThatOpens = nullptr;
  
  UPROPERTY(EditAnywhere)
  float DoorCloseDelay = 2.f;

  UPROPERTY()
  class UAudioComponent* AudioComponent = nullptr;

  float DoorLastOpened = 0.f;

  FRotator InitialRotation;
  FRotator TargetRotation;

  bool OpenDoorSound = false;
  bool CloseDoorSound = false;
};
