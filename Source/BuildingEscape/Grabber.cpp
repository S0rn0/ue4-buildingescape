// Copyright © 2021 Derek Fletcher, All rights reserved

#include "Grabber.h"
#include "BuildingEscape.h"

#include "DrawDebugHelpers.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

  PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
  if (PhysicsHandle)
  {
    GetOwner()->InputComponent->BindAction(TEXT("Grab"), IE_Pressed, this, &UGrabber::Grab);
    GetOwner()->InputComponent->BindAction(TEXT("Grab"), IE_Released, this, &UGrabber::Release);
  }
  else 
  {
    UE_LOG(LogBuildingEscape, Error, TEXT("No physics handle component found on grabber component owner %s"), *GetOwner()->GetName());
  }
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  if (PhysicsHandle->GetGrabbedComponent())
  {
    FVector PlayerViewPointLocation;
    FRotator PlayerViewPointRotation;
    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT PlayerViewPointLocation, OUT PlayerViewPointRotation);
    const FVector LineTraceEnd = PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;

    PhysicsHandle->SetTargetLocation(LineTraceEnd);
  }
}

void UGrabber::Grab()
{
  FVector PlayerViewPointLocation;
  FRotator PlayerViewPointRotation;
  GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT PlayerViewPointLocation, OUT PlayerViewPointRotation);
  const FVector LineTraceEnd = PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;

  FHitResult Hit;
  if (
    GetWorld()->LineTraceSingleByObjectType(
      OUT Hit, PlayerViewPointLocation,
      LineTraceEnd,
      FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
      FCollisionQueryParams(FName(TEXT("")), false, GetOwner())
    )
  )
  {
    UE_LOG(LogTemp, Warning, TEXT("%s"), *Hit.GetActor()->GetName());
    PhysicsHandle->GrabComponentAtLocation(Hit.GetComponent(), NAME_None, Hit.GetActor()->GetActorLocation());
  }
}

void UGrabber::Release()
{
  PhysicsHandle->ReleaseComponent();
}
