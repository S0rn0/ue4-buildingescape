// Copyright © 2021 Derek Fletcher, All rights reserved

#include "DoorController.h"
#include "BuildingEscape.h"

#include "Components/AudioComponent.h"
#include "Engine/TriggerVolume.h"

// Sets default values for this component's properties
UDoorController::UDoorController()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UDoorController::BeginPlay()
{
	Super::BeginPlay();
  InitialRotation = GetOwner()->GetActorRotation();
  TargetRotation = FRotator(GetOwner()->GetActorRotation().Pitch + DeltaPitch, GetOwner()->GetActorRotation().Yaw + DeltaYaw, GetOwner()->GetActorRotation().Roll + DeltaRoll);

  if (!PressurePlate)
  {
    UE_LOG(LogBuildingEscape, Error, TEXT("No trigger volume found on door controller component that is attached to %s"), *GetOwner()->GetName());
  }

  if (!ActorThatOpens)
  {
    UE_LOG(LogBuildingEscape, Error, TEXT("No actor to trigger plate found on door controller component that is attached to %s"), *GetOwner()->GetName());
  }

  AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
  if (!AudioComponent)
  {
    UE_LOG(LogBuildingEscape, Error, TEXT("No audio component found on grabber component owner %s"), *GetOwner()->GetName());
  }
}

void UDoorController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  if (PressurePlate && ActorThatOpens && PressurePlate->IsOverlappingActor(ActorThatOpens))
  {
    OpenDoor(DeltaTime);
    DoorLastOpened = GetWorld()->GetTimeSeconds();
  }
  else
  {
    if (GetWorld()->GetTimeSeconds() - DoorLastOpened > DoorCloseDelay)
    {
      CloseDoor(DeltaTime);
    }
  }
}

void UDoorController::OpenDoor(float DeltaTime)
{
  GetOwner()->SetActorRotation(FMath::RInterpTo(GetOwner()->GetActorRotation(), TargetRotation, OpenSpeed, DeltaTime));

  CloseDoorSound = false;
  if (AudioComponent && !OpenDoorSound)
  {
    OpenDoorSound = true;
    AudioComponent->Play();
  }
}

void UDoorController::CloseDoor(float DeltaTime)
{
  GetOwner()->SetActorRotation(FMath::RInterpTo(GetOwner()->GetActorRotation(), InitialRotation, OpenSpeed, DeltaTime));

  OpenDoorSound = false;
  if (AudioComponent && !CloseDoorSound)
  {
    CloseDoorSound = true;
    AudioComponent->Play();
  }
}
